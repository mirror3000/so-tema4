#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/statfs.h>
#include <linux/fs.h>
#include <linux/buffer_head.h>

#include "pitix.h"

MODULE_DESCRIPTION("Pitix filesystem");
MODULE_AUTHOR("Andrei Preda");
MODULE_LICENSE("GPL");

#define LOG_LEVEL KERN_INFO

struct pitix_inode_info {
	struct inode vfs_inode;
	__u16 data_blocks[0];
};

static void pitix_put_super(struct super_block *sb);
static int pitix_statfs(struct dentry *dentry, struct kstatfs *buf);
static int pitix_readpage(struct file *file, struct page *page);
static int pitix_writepage(struct page *page, struct writeback_control *wbc);
static int pitix_write_begin(struct file *file, struct address_space *mapping,
			     loff_t pos, unsigned len, unsigned flags,
			     struct page **pagep, void **fsdata);
static sector_t pitix_bmap(struct address_space *mapping, sector_t block);
static int pitix_getattr(struct vfsmount *mnt, struct dentry *dentry,
			 struct kstat *stat);
static int pitix_setattr(struct dentry *dentry, struct iattr *attr);
static int pitix_mkdir(struct inode *dir, struct dentry *dentry, umode_t mode);
static struct dentry *pitix_lookup(struct inode *dir, struct dentry *dentry,
				   unsigned int flags);
static int pitix_create(struct inode *dir, struct dentry *dentry, umode_t mode,
			bool excl);
static int pitix_truncate(struct inode *inode);
static int pitix_unlink(struct inode *dir, struct dentry *dentry);
static int pitix_rmdir(struct inode *dir, struct dentry *dentry);
static int pitix_readdir(struct file *filp, struct dir_context *ctx);
static struct inode *pitix_alloc_inode_mem(struct super_block *sb);
static void pitix_destroy_inode(struct inode *inode);
static struct dentry *pitix_mount(struct file_system_type *fs_type, int flags,
				  const char *dev_name, void *data);

static struct file_system_type pitix_fs_type = {
	.name		= "pitix",
	.fs_flags	= FS_REQUIRES_DEV,
	.mount		= pitix_mount,
	.kill_sb	= kill_block_super,
	.owner		= THIS_MODULE,
};

const struct super_operations pitix_sops = {
	.alloc_inode	= pitix_alloc_inode_mem,
	.destroy_inode	= pitix_destroy_inode,
	.evict_inode	= pitix_evict_inode,
	.write_inode	= pitix_write_inode,
	.put_super	= pitix_put_super,
	.statfs		= pitix_statfs,
};

const struct address_space_operations pitix_aops = {
	.readpage	= pitix_readpage,
	.writepage	= pitix_writepage,
	.write_begin	= pitix_write_begin,
	.write_end	= generic_write_end,
	.bmap		= pitix_bmap,
};

const struct inode_operations pitix_dir_inode_operations = {
	.mkdir		= pitix_mkdir,
	.lookup		= pitix_lookup,
	.create		= pitix_create,
	.unlink		= pitix_unlink,
	.rmdir		= pitix_rmdir,
	.getattr	= pitix_getattr,
};

const struct file_operations pitix_dir_operations = {
	.read		= generic_read_dir,
	.iterate	= pitix_readdir,
};

const struct file_operations pitix_file_operations = {
	.read		= do_sync_read,
	.aio_read	= generic_file_aio_read,
	.write		= do_sync_write,
	.aio_write	= generic_file_aio_write,
	.mmap		= generic_file_mmap,
	.fsync		= noop_fsync,
	.splice_read	= generic_file_splice_read,
	.splice_write	= generic_file_splice_write,
	.llseek		= generic_file_llseek,
};

const struct inode_operations pitix_file_inode_operations = {
	.getattr	= pitix_getattr,
	.setattr	= pitix_setattr,
};

static void pitix_put_super(struct super_block *sb)
{
	struct pitix_super_block *ps;
	struct buffer_head *bh1;
	__u16 bfree, ffree;

	ps = (struct pitix_super_block *) sb->s_fs_info;

	bfree = ps->bfree;
	ffree = ps->ffree;

	mark_buffer_dirty(ps->dmap_bh);
	brelse(ps->dmap_bh);

	mark_buffer_dirty(ps->imap_bh);
	brelse(ps->imap_bh);

	mark_buffer_dirty(ps->sb_bh);
	brelse(ps->sb_bh);

	/**
	 * This is a workaround. For some reason, ps->sb_bh doesn't update the
	 * data on disk.
	 */
	if (!sb_set_blocksize(sb, PITIX_SB_SIZE)) {
		printk(LOG_LEVEL "bad block size\n");
		return;
	}

	bh1 = sb_bread(sb, PITIX_SUPER_BLOCK);
	if (!bh1) {
		printk(LOG_LEVEL "error reading buffer_head for sb\n");
		return;
	}
	ps = (struct pitix_super_block *) bh1->b_data;
	ps->bfree = bfree;
	ps->ffree = ffree;
	mark_buffer_dirty(bh1);
	brelse(bh1);
}

static int pitix_statfs(struct dentry *dentry, struct kstatfs *buf)
{
	struct super_block *sb;
	struct pitix_super_block *ps;

	sb = dentry->d_sb;
	ps = sb->s_fs_info;

	buf->f_type = sb->s_magic;
	buf->f_bsize = sb->s_blocksize;
	buf->f_blocks = get_blocks(sb);
	buf->f_bfree = ps->bfree;
	buf->f_bavail = buf->f_bfree;
	buf->f_files = get_inodes(sb);
	buf->f_ffree = ps->ffree;
	buf->f_namelen = PITIX_NAME_LEN;

	return 0;
}

int pitix_alloc_block(struct super_block *sb)
{
	struct pitix_super_block *ps;
	int block, bno;

	bno = get_blocks(sb);
	ps = (struct pitix_super_block *) sb->s_fs_info;

	if (!ps->bfree)
		return -ENOMEM;

	block = find_first_zero_bit((unsigned long *) ps->dmap, bno);

	/* This should not happend. If it does, the formatting is bad. */
	if (block == bno) {
		printk(LOG_LEVEL "bfree not consistent with bitmap\n");
		return -ENOMEM;
	}

	set_bit(block, (unsigned long *) ps->dmap);
	mark_buffer_dirty(ps->dmap_bh);

	ps->bfree--;
	mark_buffer_dirty(ps->sb_bh);

	return block;
}

void pitix_free_block(struct super_block *sb, int block)
{
	struct pitix_super_block *ps;

	if (block < 0 || block >= get_blocks(sb))
		return;

	ps = (struct pitix_super_block *) sb->s_fs_info;

	if (!test_and_clear_bit(block, (unsigned long *) ps->dmap)) {
		printk(LOG_LEVEL "block is already free\n");
		return;
	}

	mark_buffer_dirty(ps->dmap_bh);

	ps->bfree++;
	mark_buffer_dirty(ps->sb_bh);
}

int pitix_get_block(struct inode *inode, sector_t block,
		    struct buffer_head *bh_result, int create)
{
	struct super_block *sb;
	struct pitix_inode_info *pii;
	struct pitix_super_block *ps;
	int blknew;

	sb = inode->i_sb;
	ps = (struct pitix_super_block *) sb->s_fs_info;
	pii = container_of(inode, struct pitix_inode_info, vfs_inode);

	if (block >= ps->inode_data_blocks) {
		printk(LOG_LEVEL "block too big\n");
		return -EIO;
	}

	if (create) {
		blknew = pitix_alloc_block(sb);
		if (blknew < 0) {
			printk(LOG_LEVEL "no more space for data blocks\n");
			return -EIO;
		}
		pii->data_blocks[block] = blknew;
	}

	map_bh(bh_result, sb, ps->dzone_block + pii->data_blocks[block]);

	return 0;
}

static int pitix_readpage(struct file *file, struct page *page)
{
	return block_read_full_page(page, pitix_get_block);
}

static int pitix_writepage(struct page *page, struct writeback_control *wbc)
{
	return block_write_full_page(page, pitix_get_block, wbc);
}

static void pitix_write_failed(struct address_space *mapping, loff_t to)
{
	struct inode *inode;
	loff_t i_size;

	inode = mapping->host;
	i_size = i_size_read(inode);

	if (to > i_size) {
		truncate_pagecache(inode, i_size);
		pitix_truncate(inode);
	}
}

static int pitix_write_begin(struct file *file, struct address_space *mapping,
			     loff_t pos, unsigned len, unsigned flags,
			     struct page **pagep, void **fsdata)
{
	int ret;

	ret = block_write_begin(mapping, pos, len, flags, pagep,
				pitix_get_block);
	if (ret)
		pitix_write_failed(mapping, pos + len);

	return ret;
}

static sector_t pitix_bmap(struct address_space *mapping, sector_t block)
{
	return generic_block_bmap(mapping, block, pitix_get_block);
}

static int pitix_getattr(struct vfsmount *mnt, struct dentry *dentry,
			 struct kstat *stat)
{
	struct super_block *sb;
	struct inode *inode;
	struct pitix_inode_info *pii;

	sb = dentry->d_sb;
	inode = dentry->d_inode;
	pii = container_of(inode, struct pitix_inode_info, vfs_inode);

	generic_fillattr(inode, stat);

	stat->blocks = 0;
	while (pii->data_blocks[stat->blocks])
		stat->blocks++;

	stat->blksize = sb->s_blocksize;
	return 0;
}

static int pitix_setattr(struct dentry *dentry, struct iattr *attr)
{
	struct inode *inode;
	int ret;

	inode = dentry->d_inode;

	ret = inode_change_ok(inode, attr);
	if (ret)
		return ret;

	if ((attr->ia_valid & ATTR_SIZE) &&
	    attr->ia_size != i_size_read(inode)) {
		ret = inode_newsize_ok(inode, attr->ia_size);
		if (ret)
			return ret;

		truncate_setsize(inode, attr->ia_size);
		ret = pitix_truncate(inode);
		if (ret)
			return ret;
	}

	setattr_copy(inode, attr);
	mark_inode_dirty(inode);

	return 0;
}

struct inode *pitix_new_inode(struct super_block *sb)
{
	struct inode *inode;
	int ino;

	ino = pitix_alloc_inode(sb);
	if (ino < 0) {
		printk(LOG_LEVEL "no space left in imap\n");
		return NULL;
	}

	inode = new_inode(sb);
	if (!inode) {
		printk(LOG_LEVEL "new_inode failed\n");
		pitix_free_inode(sb, ino);
		return NULL;
	}

	inode->i_uid	= current_fsuid();
	inode->i_gid	= current_fsgid();
	inode->i_ino	= ino;
	inode->i_blocks	= 0;
	inode->i_mtime = inode->i_atime = inode->i_ctime = CURRENT_TIME;
	inode->i_mapping->a_ops = &pitix_aops;

	insert_inode_hash(inode);

	return inode;
}

static int pitix_add_link(struct dentry *dentry, struct inode *inode)
{
	struct buffer_head *bh;
	struct inode *dir;
	struct pitix_inode_info *pii;
	struct super_block *sb;
	struct pitix_super_block *ps;
	struct pitix_dir_entry *pdes;
	int i, ret, pitix_num_entries;

	dir = dentry->d_parent->d_inode;
	pii = container_of(dir, struct pitix_inode_info, vfs_inode);
	sb = dir->i_sb;
	ps = sb->s_fs_info;
	ret = 0;
	pitix_num_entries = dir_entries_per_block(sb);

	bh = sb_bread(sb, ps->dzone_block + pii->data_blocks[0]);
	if (!bh) {
		ret = -EIO;
		printk(LOG_LEVEL "could not read block\n");
		goto out_bad_sb;
	}

	pdes = (struct pitix_dir_entry *) bh->b_data;

	for (i = 0; i < pitix_num_entries && pdes[i].ino != 0; i++)
		;

	if (i == pitix_num_entries) {
		ret = -ENOSPC;
		printk(LOG_LEVEL "directory full\n");
		goto out;
	}

	pdes[i].ino = inode->i_ino;
	memcpy(pdes[i].name, dentry->d_name.name, PITIX_NAME_LEN);
	dir->i_mtime = dir->i_ctime = CURRENT_TIME;

	mark_buffer_dirty(bh);
out:
	brelse(bh);
out_bad_sb:
	return ret;
}

static int pitix_mkdir(struct inode *dir, struct dentry *dentry, umode_t mode)
{
	return pitix_create(dir, dentry, S_IFDIR | mode, 0);
}

static struct dentry *pitix_lookup(struct inode *dir, struct dentry *dentry,
				   unsigned int flags)
{
	struct pitix_inode_info *pii;
	struct super_block *sb;
	struct pitix_super_block *ps;
	struct pitix_dir_entry *pde;
	struct buffer_head *bh;
	struct inode *inode;
	struct dentry *ret;
	int i, found;

	pii = container_of(dir, struct pitix_inode_info, vfs_inode);
	sb = dir->i_sb;
	ps = sb->s_fs_info;
	found = 0;
	ret = NULL;
	inode = NULL;

	dentry->d_op = sb->s_root->d_op;

	bh = sb_bread(sb, ps->dzone_block + pii->data_blocks[0]);
	if (!bh) {
		printk(LOG_LEVEL "could not read block\n");
		return ERR_PTR(-EIO);
	}

	for (i = 0; i < dir_entries_per_block(sb); i++) {
		pde = ((struct pitix_dir_entry *) bh->b_data) + i;
		if (pde->ino != 0) {
			if (!strcmp(dentry->d_name.name, pde->name)) {
				found = 1;
				break;
			}
		}
	}

	if (found) {
		inode = pitix_iget(sb, pde->ino);
		if (IS_ERR(inode)) {
			ret = ERR_CAST(inode);
			goto out;
		}
	}

	d_add(dentry, inode);

out:
	brelse(bh);
	return ret;
}

static int pitix_create(struct inode *dir, struct dentry *dentry, umode_t mode,
			bool excl)
{
	struct pitix_inode_info *pii;
	struct super_block *sb;
	struct pitix_super_block *ps;
	struct inode *inode;
	struct buffer_head *bh;
	int ret;

	sb = dir->i_sb;

	inode = pitix_new_inode(sb);
	if (!inode) {
		printk(LOG_LEVEL "error allocating new inode\n");
		return -ENOMEM;
	}

	inode->i_mode = mode;

	if (S_ISREG(mode)) {
		inode->i_op	= &pitix_file_inode_operations;
		inode->i_fop	= &pitix_file_operations;
	} else if (S_ISDIR(mode)) {
		inode->i_op = &pitix_dir_inode_operations;
		inode->i_fop = &pitix_dir_operations;

		truncate_setsize(inode, sb->s_blocksize);
		ret = pitix_truncate(inode);
		if (ret) {
			printk(LOG_LEVEL "cannot truncate dir inode\n");
			goto err;
		}

		ps = sb->s_fs_info;
		pii = container_of(inode, struct pitix_inode_info, vfs_inode);

		bh = sb_bread(sb, ps->dzone_block + pii->data_blocks[0]);
		if (!bh) {
			printk(LOG_LEVEL "could not read block\n");
			ret = -EIO;
			goto err;
		}
		memset(bh->b_data, 0, sb->s_blocksize);
		mark_buffer_dirty(bh);
		brelse(bh);
	} else {
		printk(LOG_LEVEL "neither dir or file\n");
		ret = -EINVAL;
		goto err;
	}

	ret = pitix_add_link(dentry, inode);
	if (ret) {
		printk(LOG_LEVEL "add link failed\n");
		goto err;
	}

	d_instantiate(dentry, inode);
	mark_inode_dirty(inode);

	return 0;

err:
	inode_dec_link_count(inode);
	iput(inode);

	return ret;
}

static int pitix_unlink(struct inode *dir, struct dentry *dentry)
{
	struct inode *inode;
	struct buffer_head *bh;
	struct super_block *sb;
	struct pitix_inode_info *pii;
	struct pitix_super_block *ps;
	struct pitix_dir_entry *pdes;
	int i, ret;

	ret = -ENOENT;

	sb = dir->i_sb;
	ps = sb->s_fs_info;
	pii = container_of(dir, struct pitix_inode_info, vfs_inode);
	inode = dentry->d_inode;

	bh = sb_bread(sb, ps->dzone_block + pii->data_blocks[0]);
	if (!bh) {
		printk(LOG_LEVEL "could not read block\n");
		return -EIO;
	}

	pdes = (struct pitix_dir_entry *) bh->b_data;
	for (i = 0; i < dir_entries_per_block(sb); i++) {
		if (pdes[i].ino != 0) {
			if (!strcmp(dentry->d_name.name, pdes[i].name)) {
				pdes[i].ino = 0;
				mark_buffer_dirty(bh);
				inode_dec_link_count(inode);
				ret = 0;
				break;
			}
		}
	}

	brelse(bh);
	return ret;
}

static int pitix_empty_dir(struct inode *inode)
{
	struct pitix_inode_info *pii;
	struct super_block *sb;
	struct pitix_super_block *ps;
	struct pitix_dir_entry *pdes;
	struct buffer_head *bh;
	int i;

	pii = container_of(inode, struct pitix_inode_info, vfs_inode);
	sb = inode->i_sb;
	ps = sb->s_fs_info;

	bh = sb_bread(sb, ps->dzone_block + pii->data_blocks[0]);
	if (!bh) {
		printk(LOG_LEVEL "could not read block\n");
		return 0;
	}

	pdes = (struct pitix_dir_entry *) bh->b_data;
	for (i = 0; i < dir_entries_per_block(sb); i++)
		if (pdes[i].ino != 0) {
			brelse(bh);
			return 0;
		}

	brelse(bh);

	return 1;
}

static int pitix_rmdir(struct inode *dir, struct dentry *dentry)
{
	if (pitix_empty_dir(dentry->d_inode))
		return pitix_unlink(dir, dentry);
	else
		return -ENOTEMPTY;
}

static int pitix_readdir(struct file *filp, struct dir_context *ctx)
{
	struct buffer_head *bh;
	struct pitix_dir_entry *pdes;
	struct inode *inode;
	struct pitix_inode_info *pii;
	struct super_block *sb;
	struct pitix_super_block *ps;
	int over, ret;

	ret = 0;
	inode = file_inode(filp);
	pii = container_of(inode, struct pitix_inode_info, vfs_inode);
	sb = inode->i_sb;
	ps = sb->s_fs_info;

	bh = sb_bread(sb, ps->dzone_block + pii->data_blocks[0]);
	if (!bh) {
		printk(LOG_LEVEL "could not read block\n");
		ret = -ENOMEM;
		goto out_bad_sb;
	}

	pdes = (struct pitix_dir_entry *) bh->b_data;

	for (; ctx->pos < dir_entries_per_block(sb); ctx->pos++)
		if (pdes[ctx->pos].ino != 0) {
			over = dir_emit(ctx, pdes[ctx->pos].name,
					PITIX_NAME_LEN, pdes[ctx->pos].ino,
					DT_UNKNOWN);
			if (over) {
				ctx->pos++;
				goto done;
			}
		}

done:
	brelse(bh);
out_bad_sb:
	return ret;
}

int pitix_alloc_inode(struct super_block *sb)
{
	struct pitix_super_block *ps;
	int inode, ino;

	ino = get_inodes(sb);
	ps = (struct pitix_super_block *) sb->s_fs_info;

	if (!ps->ffree)
		return -ENOMEM;

	inode = find_first_zero_bit((unsigned long *) ps->imap, ino);

	/* This should not happend. If it does, the formatting is bad. */
	if (inode == ino) {
		printk(LOG_LEVEL "ffree not consistent with bitmap\n");
		return -ENOMEM;
	}

	set_bit(inode, (unsigned long *) ps->imap);
	mark_buffer_dirty(ps->imap_bh);

	ps->ffree--;
	mark_buffer_dirty(ps->sb_bh);

	return inode;
}

void pitix_free_inode(struct super_block *sb, int ino)
{
	struct pitix_super_block *ps;

	if (ino < 0 || ino >= get_blocks(sb))
		return;

	ps = (struct pitix_super_block *) sb->s_fs_info;

	if (!test_and_clear_bit(ino, (unsigned long *) ps->imap)) {
		printk(LOG_LEVEL "inode is already free\n");
		return;
	}

	mark_buffer_dirty(ps->imap_bh);

	ps->ffree++;
	mark_buffer_dirty(ps->sb_bh);
}

static struct inode *pitix_alloc_inode_mem(struct super_block *sb)
{
	struct pitix_inode_info *pii;

	pii = kzalloc(sizeof(*pii), GFP_KERNEL);
	if (!pii) {
		printk(LOG_LEVEL "error allocing mem for pii\n");
		return NULL;
	}

	inode_init_once(&pii->vfs_inode);

	return &pii->vfs_inode;
}

static void pitix_destroy_inode(struct inode *inode)
{
	kfree(container_of(inode, struct pitix_inode_info, vfs_inode));
}

static int pitix_truncate(struct inode *inode)
{
	struct super_block *sb;
	struct pitix_inode_info *pii;
	loff_t newsize;
	int newblocks, rem, i, ret;

	sb = inode->i_sb;
	pii = container_of(inode, struct pitix_inode_info, vfs_inode);

	newsize = i_size_read(inode);
	rem = do_div(newsize, sb->s_blocksize);
	newblocks = newsize + (rem ? 1 : 0);

	for (i = 0; i < newblocks; i++)
		if (!pii->data_blocks[i]) {
			ret = pitix_alloc_block(sb);
			if (ret < 0) {
				printk(LOG_LEVEL "truncate failed, no space\n");
				return -ENOMEM;
			}
			pii->data_blocks[i] = ret;
		}

	while (pii->data_blocks[i]) {
		pitix_free_block(sb, pii->data_blocks[i]);
		pii->data_blocks[i] = 0;
		i++;
	}

	block_truncate_page(inode->i_mapping, inode->i_size, pitix_get_block);

	inode->i_mtime = inode->i_ctime = CURRENT_TIME_SEC;

	mark_inode_dirty(inode);

	return 0;
}

void pitix_evict_inode(struct inode *inode)
{
	truncate_inode_pages(&inode->i_data, 0);
	if (!inode->i_nlink) {
		truncate_setsize(inode, 0);
		pitix_truncate(inode);
	}
	invalidate_inode_buffers(inode);
	clear_inode(inode);
	if (!inode->i_nlink)
		pitix_free_inode(inode->i_sb, inode->i_ino);
}

int pitix_write_inode(struct inode *inode, struct writeback_control *wbc)
{
	struct pitix_inode *pi;
	struct pitix_inode_info *pii;
	struct buffer_head *bh;
	struct super_block *sb;
	struct pitix_super_block *ps;
	unsigned long ino;
	int block, off, bytes1, bytes2, isize;

	sb = inode->i_sb;
	ps = (struct pitix_super_block *) sb->s_fs_info;
	pii = container_of(inode, struct pitix_inode_info, vfs_inode);

	pi = kzalloc(inode_size(sb), GFP_KERNEL);
	if (!pi) {
		printk(LOG_LEVEL "error allocing mem for pi\n");
		return -ENOMEM;
	}

	pi->mode	= inode->i_mode;
	pi->uid		= inode->i_uid;
	pi->gid		= inode->i_gid;
	pi->size	= inode->i_size;
	pi->time	= inode->i_mtime.tv_sec;

	memcpy(pi->data_blocks, pii->data_blocks, sizeof(__u16) *
	       ps->inode_data_blocks);

	ino = inode->i_ino;
	isize = inode_size(sb);
	block = ps->izone_block + ino * isize / sb->s_blocksize;
	off = ino * isize % sb->s_blocksize;
	bytes1 = sb->s_blocksize - off;
	bytes1 = bytes1 > isize ? isize : bytes1;
	bytes2 = isize - bytes1;

	bh = sb_bread(sb, block);
	if (!bh)
		goto out_bad_sb;
	memcpy(bh->b_data + off, pi, bytes1);
	mark_buffer_dirty(bh);
	brelse(bh);

	if (bytes2) {
		bh = sb_bread(sb, block + 1);
		if (!bh)
			goto out_bad_sb;
		memcpy(bh->b_data, (void *) pi + bytes1, bytes2);
		mark_buffer_dirty(bh);
		brelse(bh);
	}

	kfree(pi);

	return 0;

out_bad_sb:
	printk(LOG_LEVEL "write: could not read block\n");
	kfree(pi);
	return -EINVAL;
}

struct inode *pitix_iget(struct super_block *sb, unsigned long ino)
{
	struct pitix_inode *pi;
	struct pitix_inode_info *pii;
	struct pitix_super_block *ps;
	struct buffer_head *bh;
	struct inode *inode;
	int block, off, bytes1, bytes2, isize, err;

	inode = iget_locked(sb, ino);
	if (!inode) {
		printk(LOG_LEVEL "error aquiring inode\n");
		return ERR_PTR(-ENOMEM);
	}

	if (!(inode->i_state & I_NEW))
		return inode;

	ps = (struct pitix_super_block *) sb->s_fs_info;

	isize = inode_size(sb);
	block = ps->izone_block + ino * isize / sb->s_blocksize;
	off = ino * isize % sb->s_blocksize;
	bytes1 = sb->s_blocksize - off;
	bytes1 = bytes1 > isize ? isize : bytes1;
	bytes2 = isize - bytes1;

	pi = kzalloc(isize, GFP_KERNEL);
	if (!pi) {
		printk(LOG_LEVEL "error allocing mem for pi\n");
		return ERR_PTR(-ENOMEM);
	}

	bh = sb_bread(sb, block);
	if (!bh) {
		printk(LOG_LEVEL "iget: could not read block\n");
		err = -EIO;
		goto out_bad;
	}
	memcpy(pi, bh->b_data + off, bytes1);
	brelse(bh);

	if (bytes2) {
		bh = sb_bread(sb, block + 1);
		if (!bh) {
			printk(LOG_LEVEL "iget: could not read block\n");
			err = -EIO;
			goto out_bad;
		}
		memcpy((void *) pi + bytes1, bh->b_data, bytes2);
		brelse(bh);
	}

	inode->i_mode		= pi->mode;
	inode->i_uid		= pi->uid;
	inode->i_gid		= pi->gid;
	inode->i_size		= pi->size;
	inode->i_blocks		= 0;
	inode->i_mapping->a_ops	= &pitix_aops;
	inode->i_mtime = inode->i_atime = inode->i_ctime = CURRENT_TIME;

	if (S_ISDIR(inode->i_mode)) {
		inode->i_op	= &pitix_dir_inode_operations;
		inode->i_fop	= &pitix_dir_operations;
	} else if (S_ISREG(inode->i_mode)) {
		inode->i_op	= &pitix_file_inode_operations;
		inode->i_fop	= &pitix_file_operations;
	} else {
		printk(LOG_LEVEL "neither dir nor file\n");
		err = -EINVAL;
		goto out_bad;
	}

	pii = container_of(inode, struct pitix_inode_info, vfs_inode);
	memcpy(pii->data_blocks, pi->data_blocks, sizeof(__u16) *
	       ps->inode_data_blocks);

	unlock_new_inode(inode);
	kfree(pi);

	return inode;

out_bad:
	kfree(pi);
	iget_failed(inode);
	return ERR_PTR(err);
}

int pitix_fill_super(struct super_block *sb, void *data, int silent)
{
	struct buffer_head *bh1, *bh2, *bh3;
	struct pitix_super_block *ps;
	struct inode *root_inode;
	struct dentry *root_dentry;
	int ret;

	ret = -EINVAL;

	if (!sb_set_blocksize(sb, PITIX_SB_SIZE)) {
		printk(LOG_LEVEL "bad block size\n");
		goto out;
	}

	bh1 = sb_bread(sb, PITIX_SUPER_BLOCK);
	if (!bh1) {
		ret = -EIO;
		printk(LOG_LEVEL "error reading buffer_head for sb\n");
		goto out;
	}

	ps = (struct pitix_super_block *) bh1->b_data;
	sb->s_fs_info = ps;

	if (!sb_set_blocksize(sb, 1 << ps->block_size_bits)) {
		printk(LOG_LEVEL "bad block size\n");
		goto out_release1;
	}

	if (ps->magic != PITIX_MAGIC) {
		printk(LOG_LEVEL "bad magic number\n");
		goto out_release1;
	}

	sb->s_magic = PITIX_MAGIC;

	if (ps->version != PITIX_CRT_VER) {
		printk(LOG_LEVEL "bad version\n");
		goto out_release1;
	}

	sb->s_op = &pitix_sops;

	root_inode = pitix_iget(sb, PITIX_ROOT_INODE);
	if (IS_ERR(root_inode)) {
		printk(LOG_LEVEL "bad inode\n");
		goto out_release1;
	}

	root_dentry = d_make_root(root_inode);
	if (!root_dentry) {
		printk(LOG_LEVEL "make root failed\n");
		goto out_release1;
	}
	sb->s_root = root_dentry;

	/* Set the buffer heads. */
	ps->sb_bh = bh1;

	bh2 = sb_bread(sb, ps->dmap_block);
	if (!bh2) {
		ret = -EIO;
		printk(LOG_LEVEL "error reading buffer_head for dmap\n");
		goto out_release1;
	}
	ps->dmap_bh = bh2;

	bh3 = sb_bread(sb, ps->imap_block);
	if (!bh3) {
		ret = -EIO;
		printk(LOG_LEVEL "error reading buffer_head for map\n");
		goto out_release2;
	}
	ps->imap_bh = bh3;

	/* Set pointers to our maps */
	ps->dmap = ps->dmap_bh->b_data;
	ps->imap = ps->imap_bh->b_data;

	return 0;

out_release2:
	brelse(bh2);
out_release1:
	brelse(bh1);
out:
	sb->s_fs_info = NULL;
	return ret;
}

static struct dentry *pitix_mount(struct file_system_type *fs_type, int flags,
				  const char *dev_name, void *data)
{
	return mount_bdev(fs_type, flags, dev_name, data, pitix_fill_super);
}

static int __init pitix_init(void)
{
	return register_filesystem(&pitix_fs_type);
}

static void __exit pitix_exit(void)
{
	unregister_filesystem(&pitix_fs_type);
}

module_init(pitix_init);
module_exit(pitix_exit);
